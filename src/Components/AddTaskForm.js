import React from "react";

const Form = props => {
    return (
        <form>
            <input
                className="input-text"
                type="text"
                value={props.task}
                onChange={props.addForm}
                required
            />
        </form>
    );
};

export default Form;