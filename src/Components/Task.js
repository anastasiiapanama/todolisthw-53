import React from 'react';

const Task = props => {
    return (
        <div className="task-item">
            <h4>{props.task}</h4>
            <button type="submit" onClick={props.remove}>Delete</button>
        </div>
    );
};

export default Task;