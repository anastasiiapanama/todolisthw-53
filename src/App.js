import React, {useState} from 'react';
import './App.css';
import Task from "./Components/Task";
import Form from "./Components/AddTaskForm";

const App = () => {
  const [tasks, setTasks] = useState([
    {text: 'Do my homework', id: 't1'},
    {text: 'Meet my friend from the airport', id: 't2'},
    {text: 'Feed the cat', id: 't3'},
    {text: 'Meditate', id: 't4'},
  ]);

  const [addTask, setAddTask] = useState(true);

  const [currentTask, setCurrentTask] = useState("");

  const onChangeHandler = event => {
    const inputValue = event.target.value;

    setCurrentTask(inputValue);
  }

  const addTaskForm = event => {
    const tasksCopy = [...tasks];
    const taskCopy = {...tasksCopy[1]};
    taskCopy.text = event.target.value;
    taskCopy.id = 't5';

    taskCopy[1] = taskCopy;

    setTasks(taskCopy);
  };

  const removeTask = id => {
    const index = tasks.findIndex(tasks => tasks.id === id)
    const tasksCopy = [...tasks];
    tasksCopy.splice(index, 1);
    setTasks(tasksCopy);
  }

  const toggleTask = () => {
    setAddTask(!addTask);
  };

  let taskList = null;

  if(addTask) {
    taskList = (
        <>
          {tasks.map((task, index) => {
            return <Task
                key={tasks.id}
                task={task.text}
                remove={() => {removeTask(tasks.id)}}
            />
          })}
        </>
    )
  }

  return (
    <div className="App">
      <div className="form-item">
        <Form
            addForm={event => onChangeHandler(event)}
            task={currentTask}
        />
        <button type="submit" onClick={addTaskForm}>Add</button>
      </div>

      <div className="task-list">
        {taskList}
      </div>
    </div>
  );
}

export default App;
